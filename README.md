# Запуск приложения (docker)

* `docker build -t nek .`
* `docker run -e SECRET_KEY='top_secret' -p 8000:8000 -it nek`
* в браузере http://127.0.0.1:8000/admin , логин:пароль - admin:admin

# Запуск тестов (docker)

* `docker build -t nek .`
* `docker run -t --rm nek python3 manage.py test`

# Разработка

* создать virtualenv
* pip install -r requirements.txt
* создать settings/local.py
* python manage.py migrate
* python manage.py runserver
* python manage.py test
