from django.conf import settings
from django.core import mail
from django.template import loader, Context

from app.models import (
    Subscription,
    Blog,
    Entry,
    UserEntry,
)


def on_entry_create(entry_id):
    entry = Entry.objects.get(id=entry_id)

    email_message_template = loader.get_template('entry/create_message.txt')
    email_message_context = Context({
        'entry': entry
    })
    email_message = email_message_template.render(email_message_context)

    email_subject_template = loader.get_template('entry/create_subject.txt')
    email_subject_context = Context({
        'entry': entry
    })
    email_subject = email_subject_template.render(email_subject_context)

    for subscription in Subscription.objects.filter(
        blog_id=entry.blog_id
    ):
        mail.send_mail(
            email_subject,
            email_message,
            from_email=settings.FROM_EMAIL,
            recipient_list=[subscription.user.email],
        )


def subscribe(user_id, blog_id):
    Subscription.objects.get_or_create(
        user_id=user_id,
        blog_id=blog_id,
    )


def unsubscribe(user_id, blog_id):
    Subscription.objects.filter(
        user_id=user_id,
        blog_id=blog_id,
    ).delete()

    UserEntry.objects.filter(
        user_id=user_id,
        entry__blog_id=blog_id,
    ).delete()


def get_entry_list(user_id):
    blog_id_list = Subscription.objects.filter(
        user_id=user_id
    ).values_list('blog_id', flat=True)
    entry_qs = Entry.objects.filter(
        blog_id__in=blog_id_list
    ).order_by('-dt_create')
    entry_list = list(entry_qs)
    mark_read_entry_id_set = set(UserEntry.objects.filter(
        user_id=user_id
    ).values_list('entry_id', flat=True))

    for entry in entry_list:
        entry.mark_read = entry.id in mark_read_entry_id_set

    return entry_list


def get_blog_list(user_id):
    subscribed_blog_id_list = set(Subscription.objects.filter(
        user_id=user_id
    ).values_list('blog_id', flat=True))
    blog_qs = Blog.objects.all().select_related('user')
    blog_list = list(blog_qs)
    for blog in blog_list:
        blog.subscribed = blog.id in subscribed_blog_id_list
    return blog_list


def set_mark_read(user_id, entry_id):
    UserEntry.objects.get_or_create(
        user_id=user_id,
        entry_id=entry_id,
        mark_read=True,
    )
