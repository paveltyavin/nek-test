from django.views.generic.base import TemplateView, RedirectView

from app.logic import (
    get_entry_list,
    get_blog_list,
    subscribe,
    set_mark_read,
    unsubscribe,
)
from app.models import Entry


class HomePage(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)

        if self.request.user.is_authenticated():
            ctx['entry_list'] = get_entry_list(self.request.user.id)
            ctx['blog_list'] = get_blog_list(self.request.user.id)
        else:
            ctx['entry_list'] = []
            ctx['blog_list'] = []

        return ctx


class EntryView(TemplateView):
    template_name = 'entry.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['entry'] = Entry.objects.get(id=kwargs['entry_id'])
        return ctx


class SubscribeView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        if self.request.user.is_authenticated():
            subscribe(self.request.user.id, kwargs['blog_id'])
        return '/'


class UnSubscribeView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        if self.request.user.is_authenticated():
            unsubscribe(self.request.user.id, kwargs['blog_id'])
        return '/'


class MarkReadView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        if self.request.user.is_authenticated():
            set_mark_read(self.request.user.id, kwargs['entry_id'])
        return '/'
