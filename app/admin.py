from django.contrib import admin
from django.contrib.auth.models import Group

from app.models import (
    User,
    Entry,
    Blog,
    Subscription,
)


class SubscriptionInline(admin.TabularInline):
    model = Subscription
    extra = 0


class UserAdmin(admin.ModelAdmin):
    inlines = [SubscriptionInline]


admin.site.unregister(Group)

admin.site.register(User, UserAdmin)
admin.site.register(Entry, None)
admin.site.register(Blog, None)
