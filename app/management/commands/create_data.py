from django.core.management.base import BaseCommand
from django.utils import timezone

from app.models import (
    User,
    Blog,
    Entry)


class Command(BaseCommand):
    def handle(self, *args, **options):
        if not User.objects.filter(
            username='admin'
        ).exists():
            User.objects.create_superuser(
                username='admin',
                password='admin',
                email='admin@example.com',
            )

        for x in range(3):
            if not User.objects.filter(
                username='user_{}'.format(x)
            ).exists():
                user = User.objects.create_user(
                    username='user_{}'.format(x),
                    password='user_{}'.format(x),
                    email='user_{}@example.com'.format(x),
                )
                blog = Blog.objects.create(user=user)

                for y in range(3):
                    Entry.objects.create(
                        blog=blog,
                        title='title {}'.format(y),
                        text='text text {}'.format(y),
                        dt_create=timezone.now(),
                    )
