# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-01-14 05:55
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_auto_20170114_0519'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subscription',
            name='dt_create',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
