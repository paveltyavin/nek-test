from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    pass


class Blog(models.Model):
    user = models.OneToOneField('User')

    def __str__(self):
        return str(self.id)


class Subscription(models.Model):
    user = models.ForeignKey('User', related_name='subscription_user')
    blog = models.ForeignKey('Blog', related_name='subscription_blog')
    dt_create = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('user', 'blog',)

    def __str__(self):
        return str(self.id)


class Entry(models.Model):
    blog = models.ForeignKey('Blog')
    dt_create = models.DateTimeField(auto_created=True)
    text = models.TextField(default='')
    title = models.CharField(default='', max_length=128)

    def __str__(self):
        return str(self.title or self.id)

    def save(self, *args, **kwargs):
        from app.logic import on_entry_create

        created = self.id is None
        super().save(*args, **kwargs)
        if created:
            on_entry_create(self.id)


class UserEntry(models.Model):
    user = models.ForeignKey('User')
    entry = models.ForeignKey('Entry')
    mark_read = models.BooleanField(default=False)

    class Meta:
        unique_together = ('user', 'entry',)

    def __str__(self):
        return '{} {}'.format(self.user_id, self.entry_id)
