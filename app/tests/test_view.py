import datetime
from unittest.mock import patch

from django.test import TestCase
from django.urls.base import reverse

from app.logic import (
    subscribe,
)
from app.models import (
    User,
    Blog,
    Entry,
)


class ViewsTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.user_1 = User.objects.create(username='username 1', email='user_1@example.com')
        self.user_2 = User.objects.create(username='username 2', email='user_2@example.com')
        self.user_3 = User.objects.create(username='username 3', email='user_3@example.com')
        self.blog_1 = Blog.objects.create(user=self.user_1)
        self.blog_2 = Blog.objects.create(user=self.user_2)
        self.blog_3 = Blog.objects.create(user=self.user_3)

        self.entry_1 = Entry.objects.create(blog=self.blog_1, dt_create=datetime.date(2010, 1, 1))
        self.entry_2 = Entry.objects.create(blog=self.blog_1, dt_create=datetime.date(2010, 1, 2))
        self.entry_3 = Entry.objects.create(blog=self.blog_1, dt_create=datetime.date(2010, 1, 3))

        self.entry_4 = Entry.objects.create(blog=self.blog_2, dt_create=datetime.date(2010, 1, 4))
        self.entry_5 = Entry.objects.create(blog=self.blog_2, dt_create=datetime.date(2010, 1, 3))
        self.entry_6 = Entry.objects.create(blog=self.blog_2, dt_create=datetime.date(2010, 1, 2))

        self.entry_7 = Entry.objects.create(blog=self.blog_3, dt_create=datetime.date(2010, 1, 7))
        self.entry_8 = Entry.objects.create(blog=self.blog_3, dt_create=datetime.date(2010, 1, 8))
        self.entry_9 = Entry.objects.create(blog=self.blog_3, dt_create=datetime.date(2010, 1, 9))

        subscribe(self.user_1.id, self.blog_2.id)
        subscribe(self.user_1.id, self.blog_3.id)

        self.client.force_login(self.user_1)

    def test_anon(self):
        self.client.logout()
        url = reverse('home')
        r = self.client.get(url)
        self.assertEqual(r.status_code, 200)

    def test_homepage(self):
        url = reverse('home')
        r = self.client.get(url)
        self.assertEqual(r.status_code, 200)

    @patch('app.views.set_mark_read')
    def test_mark_read(self, mock_func):
        url = reverse('mark_read', args=(self.entry_4.id,))
        r = self.client.get(url)
        self.assertEqual(r.status_code, 302)
        self.assertTrue(mock_func.called)

    @patch('app.views.subscribe')
    def test_subscribe(self, mock_func):
        url = reverse('subscribe', args=(self.blog_1.id,))
        r = self.client.get(url)
        self.assertEqual(r.status_code, 302)
        self.assertTrue(mock_func.called)

    @patch('app.views.unsubscribe')
    def test_unsubscribe(self, mock_func):
        url = reverse('unsubscribe', args=(self.blog_1.id,))
        r = self.client.get(url)
        self.assertEqual(r.status_code, 302)
        self.assertTrue(mock_func.called)
