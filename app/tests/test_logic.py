import datetime

from django.test import TestCase
from django.core import mail

from app.logic import (
    subscribe,
    unsubscribe,
    get_entry_list,
    set_mark_read,
)
from app.models import (
    User,
    Blog,
    Subscription,
    Entry,
    UserEntry,
)


class SubcriptionTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.user_1 = User.objects.create(username='username 1', email='user_1@example.com')
        self.user_2 = User.objects.create(username='username 2', email='user_2@example.com')
        self.blog_1 = Blog.objects.create(user=self.user_1)
        self.blog_2 = Blog.objects.create(user=self.user_2)
        self.entry = Entry.objects.create(blog=self.blog_2, dt_create=datetime.date(2010, 1, 2))

    def test_subscribe(self):
        subscribe(self.user_1.id, self.blog_2.id)
        self.assertEqual(Subscription.objects.all().count(), 1)
        Subscription.objects.get(
            user=self.user_1,
            blog=self.blog_2,
        )

    def test_unsubscribe(self):
        Subscription.objects.create(
            user=self.user_1,
            blog=self.blog_2,
        )
        UserEntry.objects.create(
            user=self.user_1,
            entry=self.entry,
            mark_read=True,
        )

        unsubscribe(self.user_1.id, self.blog_2.id)
        self.assertEqual(Subscription.objects.all().count(), 0)
        self.assertEqual(UserEntry.objects.filter(
            user=self.user_1,
            entry=self.entry,
        ).count(), 0)


class EntryListTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.user_1 = User.objects.create(username='username 1', email='user_1@example.com')
        self.user_2 = User.objects.create(username='username 2', email='user_2@example.com')
        self.user_3 = User.objects.create(username='username 3', email='user_3@example.com')
        self.blog_1 = Blog.objects.create(user=self.user_1)
        self.blog_2 = Blog.objects.create(user=self.user_2)
        self.blog_3 = Blog.objects.create(user=self.user_3)

        self.entry_1 = Entry.objects.create(blog=self.blog_1, dt_create=datetime.date(2010, 1, 1))
        self.entry_2 = Entry.objects.create(blog=self.blog_1, dt_create=datetime.date(2010, 1, 2))
        self.entry_3 = Entry.objects.create(blog=self.blog_1, dt_create=datetime.date(2010, 1, 3))

        self.entry_4 = Entry.objects.create(blog=self.blog_2, dt_create=datetime.date(2010, 1, 4))
        self.entry_5 = Entry.objects.create(blog=self.blog_2, dt_create=datetime.date(2010, 1, 3))
        self.entry_6 = Entry.objects.create(blog=self.blog_2, dt_create=datetime.date(2010, 1, 2))

        self.entry_7 = Entry.objects.create(blog=self.blog_3, dt_create=datetime.date(2010, 1, 7))
        self.entry_8 = Entry.objects.create(blog=self.blog_3, dt_create=datetime.date(2010, 1, 8))
        self.entry_9 = Entry.objects.create(blog=self.blog_3, dt_create=datetime.date(2010, 1, 9))

        subscribe(self.user_1.id, self.blog_2.id)
        subscribe(self.user_1.id, self.blog_3.id)

    def test_get_entry_list(self):
        entry_list = get_entry_list(self.user_1.id)
        self.assertEqual(
            entry_list,
            [
                self.entry_9,
                self.entry_8,
                self.entry_7,

                self.entry_4,
                self.entry_5,
                self.entry_6,
            ]
        )


class SetMarkReadTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.user_1 = User.objects.create(username='username 1', email='user_1@example.com')
        self.user_2 = User.objects.create(username='username 2', email='user_2@example.com')
        self.blog_1 = Blog.objects.create(user=self.user_1)
        self.blog_2 = Blog.objects.create(user=self.user_2)

        self.entry_1 = Entry.objects.create(blog=self.blog_1, dt_create=datetime.date(2010, 1, 1))
        self.entry_2 = Entry.objects.create(blog=self.blog_1, dt_create=datetime.date(2010, 1, 2))
        self.entry_3 = Entry.objects.create(blog=self.blog_1, dt_create=datetime.date(2010, 1, 3))

        self.entry_4 = Entry.objects.create(blog=self.blog_2, dt_create=datetime.date(2010, 1, 4))
        self.entry_5 = Entry.objects.create(blog=self.blog_2, dt_create=datetime.date(2010, 1, 3))
        self.entry_6 = Entry.objects.create(blog=self.blog_2, dt_create=datetime.date(2010, 1, 2))

    def test_mark_read(self):
        def _test(count):
            self.assertEqual(
                UserEntry.objects.filter(
                    user=self.user_1,
                    entry=self.entry_1,
                    mark_read=True,
                ).count(),
                count,
            )

        _test(0)
        set_mark_read(
            self.user_1.id,
            self.entry_1.id,
        )
        _test(1)


class EntryTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.user_1 = User.objects.create(username='username 1', email='user_1@example.com')
        self.user_2 = User.objects.create(username='username 2', email='user_2@example.com')
        self.user_3 = User.objects.create(username='username 3', email='user_3@example.com')
        self.blog = Blog.objects.create(user=self.user_1)

    def test_entry_create(self):
        Subscription.objects.create(
            user=self.user_2,
            blog=self.blog,
        )
        Subscription.objects.create(
            user=self.user_3,
            blog=self.blog,
        )
        Entry.objects.create(
            blog=self.blog,
            dt_create=datetime.date(2010, 1, 1),
        )
        self.assertEqual(len(mail.outbox), 2)
        self.assertSetEqual(
            {
                m.to[0] for m in mail.outbox
            },
            {
                'user_2@example.com',
                'user_3@example.com',
            },
        )

