#!/usr/bin/env bash

set -e

export DJANGO_SETTINGS_MODULE=settings.prod

python3 manage.py collectstatic --noinput
python3 manage.py migrate --noinput
python3 manage.py create_data

exec /usr/local/bin/gunicorn wsgi:application -w 2 -b :8000
