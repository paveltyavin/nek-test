FROM ubuntu:16.04

RUN apt-get update

RUN apt-get install -y --no-install-recommends \
        python3 \
        python3-pip \
        python3-setuptools \
    && pip3 install --upgrade pip \
    && pip3 install gunicorn \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /code
COPY requirements.txt ./
RUN pip3 install -r requirements.txt
COPY . .

RUN chmod +x docker-entrypoint.sh

EXPOSE 8000

CMD "./docker-entrypoint.sh"