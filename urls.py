from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from app import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^subscribe/(?P<blog_id>\d+)/$', views.SubscribeView.as_view(), name='subscribe'),
    url(r'^entry/(?P<entry_id>\d+)/$', views.EntryView.as_view(), name='entry'),
    url(r'^unsubscribe/(?P<blog_id>\d+)/$', views.UnSubscribeView.as_view(), name='unsubscribe'),
    url(r'^mark_read/(?P<entry_id>\d+)/$', views.MarkReadView.as_view(), name='mark_read'),
    url(r'^$', views.HomePage.as_view(), name='home'),
]

if settings.SERVE_STATIC:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
