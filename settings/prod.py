from .base import *

DEBUG = False
SERVE_STATIC = True
SECRET_KEY = os.environ['SECRET_KEY']
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
